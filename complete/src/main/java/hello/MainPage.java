package hello;

public class MainPage {
    private String content;
    private String nameContent;

    public MainPage() {
        this.nameContent = "Tu wpisz swoje imie";

        this.content = "<!DOCTYPE html>\n" +
                "<html>\n" +
                "<head>\n" +
                "\t<link href=\"https://fonts.googleapis.com/css?family=Modak|Open+Sans\" rel=\"stylesheet\">\n" +
                "\n" +
                "    <title>Technologie multimedialne</title>\n" +
                "\t<style>\n" +
                "\tbody {\n" +
                "\t\tbackground-color: #3399ff;\n" +
                "\t\tmargin: 0;\n" +
                "\t\t\n" +
                "\t}\n" +
                "\t\n" +
                "\t#header {\n" +
                "\t\tbackground-color: #3A77E1;\n" +
                "\t\t\n" +
                "\t\tfont-family: 'Modak', cursive;\n" +
                "\t\tfont-size: 40px;\n" +
                "\t\tcolor: #C2E1FC;\n" +
                "\t\t\n" +
                "\t\ttext-align: center;\n" +
                "\t\t\n" +
                "\t\tpadding: 20px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t#body {\n" +
                "\t\tfont-family: 'Open Sans', sans-serif;\n" +
                "\t\tfont-weight: bold;\n" +
                "\t\tfont-size: 20px;\n" +
                "\n" +
                "\t\tline-height: 2;\n" +
                "\t\t\n" +
                "\t\tmargin: auto;\n" +
                "\t\tmax-width: 700px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t#inputBox {\t\n" +
                "\t\tmargin-top: 40px;\n" +
                "\t\t\n" +
                "\t\ttext-indent: 50px;\n" +
                "\t\ttext-align: right;\n" +
                "\t}\n" +
                "\t\n" +
                "\tinput {\n" +
                "\t\tborder: 2px solid #CCCCCC;\n" +
                "\t\tfont-size: 20px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.button {\n" +
                "\t\tbackground-color: #001FF6;\n" +
                "\t\tborder: none;\n" +
                "\t\tcolor: white;\n" +
                "\t\tpadding: 15px 32px;\n" +
                "\t\ttext-align: center;\n" +
                "\t\ttext-decoration: none;\n" +
                "\t\tdisplay: inline-block;\n" +
                "\t\tfont-size: 16px;\n" +
                "\t\ttext-align: right;\n" +
                "\t\tmargin-top: 20px;\n" +
                "\t\tmargin-left: 230px;\n" +
                "\t\tmargin-right: 0px;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.enabledButton:hover {\n" +
                "\t\tbackground-color: #3399ff;\n" +
                "\t\tcolor: #001FF6;\n" +
                "\t\t\n" +
                "\t\ttransition: background-color .6s, color .6s ease-in-out;\n" +
                "\t}\n" +
                "\t\n" +
                "\t.errorBox {\n" +
                "\t\tdisplay: inline;\n" +
                "\t\tcolor: red;\n" +
                "\t\ttext-align: left;\n" +
                "\t}\n" +
                "\t\n" +
                "\t#sendDataMessage {\n" +
                "\t\tcolor green;\n" +
                "\t}\n" +
                "\n" +
                "\t.circle{\n" +
                "\t\tposition: absolute;\n" +
                "\t\tborder-radius: 50%;\n" +
                "\t\tbackground: white;\n" +
                "\t\tanimation: ripple 15s infinite;\n" +
                "\t\tbox-shadow: 0px 0px 1px 0px #508fb9;\n" +
                "\t}\n" +
                "\n" +
                "\t.small{\n" +
                "\t\twidth: 200px;\n" +
                "\t\theight: 200px;\n" +
                "\t\tleft: -100px;\n" +
                "\t\tbottom: -100px;\n" +
                "\t}\n" +
                "\n" +
                "\t.medium{\n" +
                "\t\twidth: 400px;\n" +
                "\t\theight: 400px;\n" +
                "\t\tleft: -200px;\n" +
                "\t\tbottom: -200px;\n" +
                "\t}\n" +
                "\n" +
                "\t.large{\n" +
                "\t\twidth: 600px;\n" +
                "\t\theight: 600px;\n" +
                "\t\tleft: -300px;\n" +
                "\t\tbottom: -300px;\n" +
                "\t}\n" +
                "\n" +
                "\t.xlarge{\n" +
                "\t\twidth: 800px;\n" +
                "\t\theight: 800px;\n" +
                "\t\tleft: -400px;\n" +
                "\t\tbottom: -400px;\n" +
                "\t}\n" +
                "\n" +
                "\t.xxlarge{\n" +
                "\t\twidth: 1000px;\n" +
                "\t\theight: 1000px;\n" +
                "\t\tleft: -500px;\n" +
                "\t\tbottom: -500px;\n" +
                "\t}\n" +
                "\n" +
                "\t.shade1{\n" +
                "\t\topacity: 0.2;\n" +
                "\t}\n" +
                "\t.shade2{\n" +
                "\t\topacity: 0.5;\n" +
                "\t}\n" +
                "\n" +
                "\t.shade3{\n" +
                "\t\topacity: 0.7;\n" +
                "\t}\n" +
                "\n" +
                "\t.shade4{\n" +
                "\t\topacity: 0.8;\n" +
                "\t}\n" +
                "\n" +
                "\t.shade5{\n" +
                "\t\topacity: 0.9;\n" +
                "\t}\n" +
                "\n" +
                "\t@keyframes ripple{\n" +
                "\t\t0%{\n" +
                "\t\t\ttransform: scale(0.8);\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t50%{\n" +
                "\t\t\ttransform: scale(1.2);\n" +
                "\t\t}\n" +
                "\n" +
                "\t\t100%{\n" +
                "\t\t\ttransform: scale(0.8);\n" +
                "\t\t}\n" +
                "\t}\n" +
                "\t\n" +
                "\t#animation {\n" +
                "\t\ttext-align: right;\n" +
                "\t\tmargin-top: 50px;\n" +
                "\t\tmargin-right: 20px;\n" +
                "\t\tdisplay: none;\n" +
                "\t}\n" +
                "\n" +
                "\t\n" +
                "\t</style>\n" +
                "</head>\n" +
                "<body>\n" +
                "\n" +
                "\t<div class=\"circle small shade5\"></div>\n" +
                "\t<div class=\"circle medium shade5\"></div>\n" +
                "\t<div class=\"circle large shade3\"></div>\n" +
                "\t<div class=\"circle xlarge shade2\"></div>\n" +
                "\t<div class=\"circle xxlarge shade1\"></div>\n" +
                "\n" +
                "\t<div id=\"header\">\n" +
                "\t\tZapis na przedmiot: Technologie Multimedialne\n" +
                "\t</div>\n" +
                "\t\n" +
                "\t<div id=\"body\">\n" +
                "\t\n" +
                "\t\t<div id=\"inputBox\">\n" +
                "\t\t\t\t\n" +
                "\t\t\t<p id=\"nameError\" class=\"errorBox \"></p>\n" +
                "\t\t\tImie: \n" +
                "\t\t\t<input id=\"name\" type=\"text\" required>\n" +
                "\t\t\t<br/>\n" +
                "\t\t\t\n" +
                "\t\t\t<p id=\"surnameError\" class=\"errorBox\"></p>\n" +
                "\t\t\tNazwisko: \n" +
                "\t\t\t<input id=\"surname\" type=\"text\" required>\n" +
                "\t\t\t<br/>\n" +
                "\t\t\t\n" +
                "\t\t\t<p id=\"indexNumberError\" class=\"errorBox\"></p>\n" +
                "\t\t\tnumer indeksu: \n" +
                "\t\t\t<input id=\"indexNumber\" type=\"text\" required>\n" +
                "\t\t\t<br/>\n" +
                "\t\t</div>\n" +
                "\t\t\n" +
                "\t\t\n" +
                "\t\t<div class=\"button enabledButton\" onclick=\"checkIfFilled()\">Prześlij</div>\n" +
                "\t\t<div class=\"button  enabledButton\" onclick=\"resetBoxes()\">Reset</div>\n" +
                "\t\t\n" +
                "\t\t<p id=\"sendDataMessage\" color=\"green\"></p>\n" +
                "\t</div>\n" +
                "\t<div id=\"animation\">\n" +
                "\t\t<IMG SRC=\"nyan.gif\">\n" +
                "\t</div>\n" +
                "\t\n" +
                "    <script>\n" +
                "\t\tfunction checkIfFilled() {\n" +
                "\t\t\tvar nameBox = document.getElementById(\"name\").value;\n" +
                "\t\t\tvar surnameBox = document.getElementById(\"surname\").value;\n" +
                "\t\t\tvar indexNumberBox = document.getElementById(\"indexNumber\").value;\n" +
                "\t\t\t\n" +
                "\t\t\tvar noSpecifiedMessage = \"*To pole jest wymagane  \"\n" +
                "\t\t\tvar numbersFoundErrorMessage = \"*To pole nie może zawierać cyfr\"\n" +
                "\t\t\tvar nanErrorMessage = \"*To pole musi się składać z cyfr\"\n" +
                "\t\t\tvar indexBadLengthMessage = \"*Indeks musi być 6-cio cyfrowy\"\n" +
                "\t\t\tvar isError = false;\n" +
                "\t\t\tvar dataSendMessage = document.getElementById(\"sendDataMessage\").innerHTML;\n" +
                "\t\t\t\n" +
                "\t\t\tif(dataSendMessage == \"\") {\n" +
                "\t\t\t\tif(nameBox == \"\") {\n" +
                "\t\t\t\t\tdocument.getElementById(\"nameError\").innerHTML = noSpecifiedMessage;\n" +
                "\t\t\t\t\tisError = true;\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\telse if(hasNumber(nameBox)) {\n" +
                "\t\t\t\t\tdocument.getElementById(\"nameError\").innerHTML = numbersFoundErrorMessage;\n" +
                "\t\t\t\t\tisError = true;\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\telse {\n" +
                "\t\t\t\t\tdocument.getElementById(\"nameError\").innerHTML = \"\";\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\t\n" +
                "\t\t\t\tif(surnameBox == \"\") {\n" +
                "\t\t\t\t\tdocument.getElementById(\"surnameError\").innerHTML = noSpecifiedMessage;\n" +
                "\t\t\t\t\tisError = true;\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\telse if(hasNumber(surnameBox)) {\n" +
                "\t\t\t\t\tdocument.getElementById(\"surnameError\").innerHTML = numbersFoundErrorMessage;\n" +
                "\t\t\t\t\tisError = true;\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\telse {\n" +
                "\t\t\t\t\tdocument.getElementById(\"surnameError\").innerHTML = \"\";\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\t\n" +
                "\t\t\t\tif(indexNumberBox == \"\") {\n" +
                "\t\t\t\t\tdocument.getElementById(\"indexNumberError\").innerHTML = noSpecifiedMessage;\n" +
                "\t\t\t\t\tisError = true;\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\telse if(isNaN(indexNumberBox)) {\n" +
                "\t\t\t\t\tdocument.getElementById(\"indexNumberError\").innerHTML = nanErrorMessage;\n" +
                "\t\t\t\t\tisError = true;\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\telse if(indexNumberBox.length != 6) {\n" +
                "\t\t\t\t\tdocument.getElementById(\"indexNumberError\").innerHTML = indexBadLengthMessage;\n" +
                "\t\t\t\t\tisError = true;\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\telse {\n" +
                "\t\t\t\t\tdocument.getElementById(\"indexNumberError\").innerHTML = \"\";\n" +
                "\t\t\t\t}\n" +
                "\t\t\t\t\n" +
                "\t\t\t\tif(!isError) {\n" +
                "\t\t\t\t\t\n" +
                "\t\t\t\t\tdocument.getElementById(\"sendDataMessage\").innerHTML = \"Dane zostały przesłane\";\n" +
                "\t\t\t\t\tdocument.getElementById(\"animation\").style.display = \"block\";\n" +
                "\t\t\t\t}\n" +
                "\t\t\t}\n" +
                "\t\t\t\n" +
                "\t\t}\n" +
                "\t\t\n" +
                "\t\tfunction resetBoxes() {\n" +
                "\t\t\tdocument.getElementById(\"name\").value = \"\";\n" +
                "\t\t\tdocument.getElementById(\"surname\").value = \"\";\n" +
                "\t\t\tdocument.getElementById(\"indexNumber\").value = \"\";\n" +
                "\t\t}\n" +
                "\t\t\n" +
                "\t\tfunction hasNumber(myString) {\n" +
                "\t\t\treturn /\\d/.test(myString);\n" +
                "\t\t}\n" +
                "    </script>\n" +
                "</body>\n" +
                "</html>";
    }

    public String getContent() {
        return content;
    }
}
