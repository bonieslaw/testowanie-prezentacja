package hello;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MainPageController {

    @RequestMapping("/start")
    public String mainPage() {
        return new MainPage().getContent();
    }
}
